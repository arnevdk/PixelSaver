﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ScreenSaver
{
    class ImageHandler
    {
       
        private Bitmap image;
        public static Blur blurMode = new PixelateBlur();

        public ImageHandler()
        {

            
        }

        public void TakeScreenShot(Screen screen)
        {
            
            Rectangle bounds = screen.WorkingArea;
            Bitmap bmpScreenCapture = new Bitmap(bounds.Width, bounds.Height + 100);
            Graphics g = Graphics.FromImage(bmpScreenCapture);
            g.CopyFromScreen(bounds.Left, bounds.Top, 0, 0, new Size(bounds.Size.Width, bounds.Size.Height + 100), CopyPixelOperation.SourceCopy);
            this.image = bmpScreenCapture;
            bmpScreenCapture.Save("D:\\sc.png", ImageFormat.Png);
        }

      
        public Image getPixelatedImage()
        {
            return blurMode.blur(getImage());
        }

        public Bitmap getImage()
        {
            return this.image;
        }

        
        private enum ProcessDPIAwareness
        {
            ProcessDPIUnaware = 0,
            ProcessSystemDPIAware = 1,
            ProcessPerMonitorDPIAware = 2
        }

        [DllImport("shcore.dll")]
        private static extern int SetProcessDpiAwareness(ProcessDPIAwareness value);

        public static void SetDpiAwareness()
        {
            try
            {
                if (Environment.OSVersion.Version.Major >= 6)
                {
                    SetProcessDpiAwareness(ProcessDPIAwareness.ProcessPerMonitorDPIAware);
                }
            }
            catch (EntryPointNotFoundException)//this exception occures if OS does not implement this API, just ignore it.
            {
            }
        }




    }
}
