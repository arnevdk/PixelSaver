﻿using Gma.System.MouseKeyHook;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

using System.Threading;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreenSaver
{
    static class Program
    {

        private static bool enabled = true;

        public static bool showingForm = false;

        private static System.Windows.Forms.Timer timer;

        private static IKeyboardMouseEvents m_GlobalHook;

        //public static int waitTime = 60 * 1000;
        public static int waitTime = 60 * 1000;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ImageHandler.SetDpiAwareness();

           
            ProcessIcon pi = new ProcessIcon();            
            pi.Display();

            m_GlobalHook = Hook.GlobalEvents();

            m_GlobalHook.MouseDownExt += mouseHandler;
            m_GlobalHook.MouseMove += mouseHandler;
            m_GlobalHook.KeyPress += keyHandler;
            m_GlobalHook.MouseWheel += mouseHandler;
            m_GlobalHook.MouseWheelExt += mouseHandler;
            m_GlobalHook.KeyDown += keyHandler;
            


            timer = new System.Windows.Forms.Timer();
            timer.Tick += new EventHandler(timerProc);
            

            restartTimer();
            Application.Run();

        }


        private static void mouseHandler(object sender, MouseEventExtArgs e)
        {

            activityHandler();
        }

        private static void mouseHandler(object sender, MouseEventArgs e)
        {
            activityHandler();
        }

        private static void keyHandler(object sender, KeyPressEventArgs e)
        {
             activityHandler();
        }

        private static void keyHandler(object sender, KeyEventArgs e)
        {
            activityHandler();
        }

        private static void activityHandler()
        {
            restartTimer();
        }

        private static void timerProc(object sender, EventArgs args)
        {
            if (showingForm)
                return;
            if (!enabled)
                restartTimer();
            else
                ShowScreenSaver();
            
        }

        public static void restartTimer()
        {
            timer.Stop();
            timer.Interval = waitTime;
            timer.Start();
        }

        public static void ShowScreenSaver()
        {
         
            showingForm = true;

            List < ScreenSaverForm > forms = new List<ScreenSaverForm>();
            foreach (Screen screen in Screen.AllScreens)
            {
                ImageHandler ih = new ImageHandler();
                ih.TakeScreenShot(screen);
                Image pixelated = ih.getPixelatedImage();
               
                ScreenSaverForm screenSaver = new ScreenSaverForm(screen.WorkingArea, pixelated);
                forms.Add(screenSaver);
            }

            foreach (ScreenSaverForm s in forms)
                s.ShowFade();
           


        }

        public static void exit()
        {
            timer.Dispose();
            Application.Exit();
        }

      

        public static void disable()
        {
            enabled = false;
        }

        public static void enable()
        {
            enabled = true;
        }
    }

    
   }
